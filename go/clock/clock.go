package clock

import "fmt"

const testVersion = 4

type Clock struct {
	hour, minute int
}

func New(hour, minute int) Clock {
	var out Clock
	// 12 hour clock
	// must take any number of hours and turn over at 12's
	out.minute = minute % 60
	out.hour = 24 + hour + (minute / 60)
	out.hour %= 24
	return out
}

func (c Clock) String() string {
	return fmt.Sprintf("%2.2d:%2.2d", c.hour, c.minute)
}

func (c Clock) Add(minutes int) Clock {
	c.minute += minutes
	c.hour = c.hour + (c.minute / 60)
	if c.hour == 24 {
		c.hour = 0
	}
	c.minute %= 60
	return c
}
